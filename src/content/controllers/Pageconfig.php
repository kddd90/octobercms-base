<?php

namespace Nitm\Content\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class Pageconfig extends Controller
{
    public $implement = ['Backend\Behaviors\ListController', 'Nitm\Content\Behaviors\FormController','Backend\Behaviors\ReorderController'];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('App.Content', 'content', 'pageconfig-menu-item');
    }
}
