<?php

namespace Nitm\Content\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class Index extends Controller
{
    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('App.Content', 'content');
    }
    public function index()
    {
    }
}
