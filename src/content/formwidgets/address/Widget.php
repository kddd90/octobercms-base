<?php

namespace Nitm\Content\FormWidgets\Address;

use Backend\Classes\FormWidgetBase;
use HTML;
use RainLab\Location\Models\Setting;

class Widget extends FormWidgetBase
{
    /**
     * {@inheritdoc}
     */
    public $defaultAlias = 'address';

    protected $fieldMap;

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        $this->fieldMap = $this->getConfig('fieldMap', []);
    }

    /**
     * {@inheritdoc}
     */
    public function render()
    {
        $this->prepareVars();

        return $this->makePartial('widget');
    }

    /**
     * Prepares the list data.
     */
    public function prepareVars()
    {
        $this->vars['name'] = $this->formField->getName();
        $this->vars['value'] = $this->getLoadValue();
        $this->vars['field'] = $this->formField;
    }

    public function getFieldMapAttributes()
    {
        $widget = $this->controller->formGetWidget();
        $fields = $widget->getFields();
        $result = [];
        foreach ($this->fieldMap as $map => $fieldName) {
            if (($field = array_get($fields, $fieldName)) != null) {
                $result['data-input-'.$map] = '#'.$field->getId();
            } else {
                $result['data-input-'.$map] = '#'.$fieldName;
            }
        }

        return HTML::attributes($result);
    }

    /**
     * {@inheritdoc}
     * Set the API key in the octopus config.
     */
    public function loadAssets()
    {
        $apiKey = Setting::get('google_maps_key');
        $mapsOptions = \Config::get('custom.google.maps') ?: [];
        $this->addJs('//maps.googleapis.com/maps/api/js?'.http_build_query(array_merge([
           'libraries' => 'places',
        ], $mapsOptions, [
           'key' => $apiKey,
        ])));
        $this->addJs('js/location-autocomplete.js', 'core');
    }
}
