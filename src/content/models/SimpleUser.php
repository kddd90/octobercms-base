<?php

namespace Nitm\Content\Models;

use Model;

/**
 * This model has limited constructon to prevent hte loading of extensions and extra relations
 * This model should be used for the related content for user actions.
 */
class SimpleUser extends RelatedUser
{
    public $eagerWith = [];
    public $with = ['avatar'];
    public $implement = [];
    public $appends = [];

   /**
    * To prevent extending the model beyond the basics.
    * Local extendable construct skips initing parent extendables.
    *
    * @method __construct
    */
   public function __construct($attributes = [])
   {
       $this->bootDefaultRelations();
       $this->bootNicerEvents();
       $this->localExtendableConstruct();
       $this->fill($attributes);
   }

    public function attributesToArray()
    {
        $result = parent::attributesToArray();
        unset($result['api_token']);

        return $result;
    }
}
