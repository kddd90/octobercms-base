<?php

namespace Nitm\Content\Models;

use Model;

/**
 * Model.
 */
class PostCount extends RelatedPost
{
    public $implement = [];
    public $with = [];
    public $eagerWith = [];
    public $visible = ['count', 'id'];
    public $appends = [];

    /**
     * To prevent extending the model beyond the basics.
     *
     * @method __construct
     */
    public function __construct()
    {
        $this->bootDefaultRelations();
    }
}
