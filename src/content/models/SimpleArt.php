<?php

namespace Nitm\Content\Models;

use Model;

/**
 * The related model minimizes the amount of data eager loaded and the amount of imformation that is implemented by extension.
 * This results in a lighter model for attached to related user actions and the user activity feed.
 */
class SimpleArt extends Art
{
    public $implement = [
   ];
    public $with = [
      'image', 'type', 'mediums', 'author', 'prices', 'collection',
   ];
    public $eagerWith = [];

    public $visible = [
      'id', 'author', 'title', 'description', 'image', 'type', 'slug', 'tags',
   ];

    public $appends = ['tags'];

   /*
    * Relations
    */
   public $belongsTo = [
      'author' => ['Nitm\Content\Models\SimpleUser', 'key' => 'author_id'],
      'user' => ['Nitm\Content\Models\SimpleUser', 'key' => 'author_id'],
      'collection' => ['Nitm\Content\Models\RelatedArtCollection', 'key' => 'collection_id'],
      'type' => [
        'Nitm\Content\Models\ArtType',
        'otherKey' => 'id',
        'key' => 'type_id',
      ],
   ];

  /**
   * To prevent extending the model beyond the basics.
   * Local extendable construct skips initing parent extendables.
   *
   * @method __construct
   */
  public function __construct($attributes = [])
  {
      $this->bootDefaultRelations();
      $this->bootNicerEvents();
      $this->localExtendableConstruct();
      $this->fill($attributes);
  }

    public function getMorphClass()
    {
        return 'Nitm\Content\Models\Art';
    }
}
