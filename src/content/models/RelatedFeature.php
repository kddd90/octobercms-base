<?php

namespace Nitm\Content\Models;

use Model;

/**
 * Model.
 */
class RelatedFeature extends Feature
{
    public $implement = [];
    public $with = [];

    public function getMorphClass()
    {
        return 'Nitm\Content\Models\Feature';
    }
}
