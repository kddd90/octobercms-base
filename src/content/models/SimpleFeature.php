<?php

namespace Nitm\Content\Models;

use Model;

/**
 * Model.
 */
class SimpleFeature extends Feature
{
    public $implement = [];
    public $with = [];

  /**
   * To prevent extending the model beyond the basics.
   * Local extendable construct skips initing parent extendables.
   *
   * @method __construct
   */
  public function __construct($attributes = [])
  {
      $this->bootDefaultRelations();
      $this->bootNicerEvents();
      $this->localExtendableConstruct();
      $this->fill($attributes);
  }

    public function getMorphClass()
    {
        return 'Nitm\Content\Models\Feature';
    }
}
