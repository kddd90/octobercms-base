<?php

namespace Nitm\Content\Models;

use Model;

/**
 * This model has limited constructon to prevent hte loading of extensions and extra relations
 * This model should be used for the related content for user actions.
 */
class RelatedActionPost extends Post
{
    public $implement = [];
    public $appends = ['date'];

    public $visible = [
      'id', 'title', 'slug', 'excerpt', 'categories', 'image', 'featured_images',
   ];

    public $with = [
       'featured_images', 'author',
    ];

   /*
    * Relations
    */
   public $belongsTo = [
      'author' => ['Nitm\Content\Models\SimpleUser', 'key' => 'user_id'],
      'user' => ['Nitm\Content\Models\SimpleUser'],
   ];

   /**
    * To prevent extending the model beyond the basics.
    *
    * @method __construct
    */
   public function __construct()
   {
       $this->bootDefaultRelations();
   }
}
