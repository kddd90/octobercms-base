<?php

namespace Nitm\Content\Models;

use Model;

/**
 * Model.
 */
class RelatedFavorite extends Favorite
{
    public $visible = ['id', 'count', 'thing_type', 'thing_id', 'thing'];
    public $with = [];
    public $appends = [];

    /**
     * To prevent extending the model beyond the basics.
     *
     * @method __construct
     */
    public function __construct()
    {
        $this->bootDefaultRelations();
    }

    public function getMorphClass()
    {
        return 'Nitm\Content\Models\Favorite';
    }

    public function relationsToArray()
    {
        if (!isset($this->attributes['count'])) {
            $result['type'] = $this->thing_type;

            return array_only($result, ['id', 'type', 'count', 'thing_id', 'thing_type', 'thing']);
        } else {
            return [];
        }
    }
}
