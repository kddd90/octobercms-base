<?php

namespace Nitm\Content\Models;

use Model;

/**
 * This model has limited constructon to prevent hte loading of extensions and extra relations
 * This model should be used for the related content for user actions.
 */
class RelatedActionEvent extends Event
{
    public $implement = [];

    public $visible = [
      'id', 'is_free', 'title', 'start', 'end', 'postponed_to', 'status', 'cost', 'type', 'category', 'description', 'location', 'image', 'date',
   ];

    public $with = [
      'image', 'type', 'category', 'locations', 'author',
   ];

   /**
    * To prevent extending the model beyond the basics.
    *
    * @method __construct
    */
   public function __construct()
   {
       $this->bootDefaultRelations();
   }

    public function getMorphClass()
    {
        return 'Nitm\Content\Models\Event';
    }
}
