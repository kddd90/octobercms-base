<?php

namespace Nitm\Content\Models;

use Model;

/**
 * Model.
 */
class RelatedFollow extends Follow
{
    public $visible = [
        'id', 'title', 'type', 'start_date', 'end_date', 'count',
     ];
    public $implements = [];
    public $with = [];
    public $appends = [];

    /**
     * To prevent extending the model beyond the basics.
     *
     * @method __construct
     */
    public function __construct()
    {
        $this->bootDefaultRelations();
    }

    public function getMorphClass()
    {
        return 'Nitm\Content\Models\Follow';
    }
}
