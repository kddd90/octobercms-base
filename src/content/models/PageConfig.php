<?php

namespace Nitm\Content\Models;

use Model;
use File;
use Lang;
use ApplicationException;
use Symfony\Component\Yaml\Dumper as YamlDumper;
use Cms\Classes\Controller;

class PageConfig extends BaseContent
{
    public static $duration = 10;
    public $indexes = [];
    public $items;
    public $table = 'nitm_content_pages';

    public $jsonable = ['config'];

    public $visible = ['config'];

    public $fillable = ['config', 'modelName', 'page', 'namespace'];
    protected $result;
    protected $pageObject;
    protected $dir;

    protected $_page;

    public static function getGroupName()
    {
        return 'PageConfig';
    }

    public function beforeCreate()
    {
        parent::beforeCreate();
        $page = preg_replace('/[^a-zA-Z0-9]/', '_', $this->page);
        $this->modelName = $this->getGroupName().ucfirst(camel_case($page));
        $this->page = 'pageconfig'.preg_replace('/[^a-zA-Z0-9]/', '', strtolower($page));
        $this->modelClass = '\\'.trim($this->namespace, '\\').'\\Models\\'.trim($this->modelName, '\\');
        $this->dir = plugins_path().'/'.ltrim($this->getNamespacedPath(), '/');
        $this->config = [];
    }

    public function afterSave()
    {
        //Delete the cached page config after saving so that it can be used immediately
        \Cache::forget(static::getCacheKey($this->page));
    }

    public static function getPage($id)
    {
        $id = strpos($id, 'pageconfig') !== false ? $id : 'pageconfig'.$id;

        return static::query()->where(['page' => $id])->first();
    }

    /**
    * get the cache key for this model
    * @param  [type] $id     [description]
    * @param  [type] $params [description]
    * @return [type]         [description]
    */
    protected static function getCacheKey($id=null, $params=[])
    {
        $id = $id ?: strtolower(class_basename(get_called_class()));
        $params = json_encode($params);
        return implode('-', array_merge((array)$params, [
            md5(get_called_class()),
            $id,
        ]));
    }

    public static function getPageObject()
    {
        return Controller::getController() ? Controller::getController()->getPageObject() : new \Cms\Classes\Page();
    }

    /**
    * Return the page configuration.
    *
    * @return array the configuration
    */
    public static function getConfig($id = null, $options = [])
    {
        $id = $id ?: strtolower(class_basename(get_called_class()));
        $routeParams = $id != 'pageconfigglobal' ? \Route::current()->parameters() : [];
        $key = static::getCacheKey($id, array_merge((array)$_GET, (array)$routeParams));

        return \Cache::remember($key, static::$duration, function () use ($id, $routeParams, $options) {
            $originalId = $id;
            $id = strtolower(preg_replace('/[^a-zA-Z0-9]/', '', $id));
            $page = static::getPage($id);
            if($page) {
                $page->pageObject = static::getPageObject();
                if ($page) {
                    $modelClass = $page->modelClass;
                    if (class_exists($modelClass)) {
                        $model = new $modelClass();
                        $model->fill($page->attributes);
                        $model->config = json_decode($model->config);
                        $args = [$page->pageObject, $options, $routeParams];

                        $ret_val = array_merge([
                        'id' => $originalId,
                        ], (array) call_user_func_array([$model, 'prepareConfig'], $args));
                        return $ret_val;
                    } else {
                        return [];
                    }
                }
            } else {
                return [];
            }
        });
    }

    protected function getNavigation()
    {
        $ret_val = [];
        if (!$this->result || ($this->result && !in_array(get_class($this->result), [
            'Illuminate\Pagination\LengthAwarePaginator',
        'Illuminate\Pagination\Paginator',
        ]))) {
            return $ret_val;
        }
        if ($this->result->nextPageUrl()) {
            $ret_val['next'] = $this->result->nextPageUrl();
        }
        if ($this->result->previousPageUrl()) {
            $ret_val['previous'] = $this->result->previousPageUrl();
        }

        return $ret_val;
    }

    protected function getPagination($items = null)
    {
        $items = $items ?: $this->items;
        if ($items && !$items instanceof \Illuminate\Pagination\LengthAwarePaginator
        || $items instanceof \Illuminate\Pagination\Paginator) {
            return [];
        }

        return [
        'last' => $items->lastPage(),
        'next' => $items->currentPage() == $items->lastPage() ? $items->lastPage() : $items->currentPage() + 1,
        'previous' => $items->currentPage() == 1 ? 1 : $items->currentPage() - 1,
        'current' => $items->currentPage(),
        ];
    }

    protected static function getShowcaseItem($config)
    {
        extract($config);

        return array_merge($config, array_filter([
        'title' => @$title,
        'isEvent' => @$isEvent,
        'isShowcase' => @$isShowcase,
        'isFeatured' => @$isFeatured,
        'featureTitle' => @$featureTitle,
        'pageTitle' => @$pageTitle,
        'image' => @$image,
        'isLocalImage' => @$isLocalImage,
        'description' => @$description,
        ]));
    }

    public function getNamespaceOptions()
    {
        $ret_val = [];
        $plugins = \System\Classes\PluginManager::instance()->getPlugins();
        foreach ($plugins as $plugin) {
            $namespace = (new \ReflectionClass($plugin))->getNamespaceName();
            $ret_val[$namespace] = $namespace;
        }

        ksort($ret_val);

        return $ret_val;
    }

    public function getPageOptions()
    {
        $models = preg_grep('/^'.static::getGroupName().'(\w+).php/', scandir(__DIR__));
        $models = array_map(function ($model) {
            return substr($this, 0, strpos($$modelthis, '.'));
        }, $models);

        return array_combine(array_map('strtolower', $this), $this);
    }

    public function afterCreate()
    {
        $dumper = new YamlDumper();
        $configArray = $this->getBaseControllerFormConfig();
        $code = $dumper->dump($configArray, 20, 0, false, true);
        $modelCode = str_replace('%s', $this->modelName, $this->getPageModelTemplate());
        $modelPath = $this->getModelPath($this).'.php';
        if (!File::exists($modelPath)) {
            $this->writeFile($modelPath, $modelCode);
        }
        $fieldConfigPath = $this->getModelConfigPath($this).'/fields.yaml';
        if (!File::exists($fieldConfigPath)) {
            $this->writeFile($fieldConfigPath, 'fields:');
        }
        $controllerFormConfigPath = $this->getControllerConfigPath($this).'/config_form.yaml';
        if (!File::exists($controllerFormConfigPath)) {
            $this->writeFile($controllerFormConfigPath, $code);
        }
    }

    public function afterDelete()
    {
        $directories = [
        $this->getModelConfigPath($this).'/fields.yaml',
        $this->getControllerConfigPath().'/config_form.yaml',
        $this->getModelPath($this).'.php'
        ];

        foreach ($directories as $directory) {
            try {
                unlink($directory);
            } catch (\Exception $e) {
            }
        }
    }

    public function afterFetch()
    {
        $this->morph();
    }

    public function morph()
    {
        $modelName = $this->modelClass;
        if (class_exists($modelName)) {
            $model = new $modelName();
            $this->hasMany = array_merge($this->hasMany, $model->hasMany);
            $this->hasOne = array_merge($this->hasOne, $model->hasOne);
        }
    }

    public function getPageNameAttribute()
    {
        return substr($this->modelName, strlen($this->getGroupName()));
    }

    public function getBaseNamespaceAttribute()
    {
        $parts = explode('\\', $this->modelClass);
        array_pop($parts);
        return trim(implode('/', array_filter($parts)), '//');
    }

    public function get()
    {
        return static::getConfig();
    }

    public function take()
    {
        return $this;
    }

    /**
    * Faker for querying the API.
    *
    * @return array the configuration
    */
    public static function apiFind($id, $options = [])
    {
        return static::getConfig($id, $options);
    }

    /**
    * Faker for querying the API.
    *
    * @return array the configuration
    */
    public static function apiQuery($options = [], $multiple = false, $query = null)
    {
        return new static();
    }

    public function prepareConfig($page, $config = [], $routeParameters = [])
    {
        throw new \Exception(__FUNCTION__.' needs to be defined by all sub classes');
    }

    public function getModel()
    {
        return $this;
    }

    public function getTable()
    {
        return $this->table;
    }

    protected function getControllerConfigPath()
    {
        return $this->dir.'/controllers/'.$this->page;
    }

    protected function getModelPath()
    {
        return $this->dir.'/models/'.$this->modelName;
    }

    protected function getModelConfigPath()
    {
        return $this->dir.'/models/'.$this->page;
    }

    /**
    * Lifted from RRainlab\Builder\Classes\ControllerGenerator.
    *
    * @param [type] $path [description]
    * @param [type] $data [description]
    *
    * @return [type] [description]
    */
    protected function writeFile($path, $data)
    {
        $fileDirectory = dirname($path);
        if (!File::isDirectory($fileDirectory)) {
            if (!File::makeDirectory($fileDirectory, 0777, true, true)) {
                throw new ApplicationException(Lang::get('rainlab.builder::lang.common.error_make_dir', [
                'name' => $fileDirectory,
                ]));
            }
        }

        if (@File::put($path, $data) === false) {
            throw new ApplicationException(Lang::get('rainlab.builder::lang.controller.error_save_file', [
            'file' => basename($path),
            ]));
        }

        @File::chmod($path);
    }

    public function getNamespacedPath()
    {
        return (str_replace('\\', '/', strtolower($this->namespace ?: __NAMESPACE__)));
    }

    protected function getYamlPath($part='')
    {
        return '$/'.$this->getNamespacedPath().'/'.($part ? $part.'/' : '');
    }

    protected function getPluginUrl()
    {
        $path = explode('/', $this->getNamespacedPath());
        array_pop($path);
        return implode('/', $path).'/';
    }

    protected function getBaseControllerFormConfig()
    {
        $pluginUrl = $this->getPluginUrl();
        return [
        'name' => 'Pageconfig',
        'form' => $this->getYamlPath('models').$this->page.'/fields.yaml',
        'modelClass' => get_class($this),
        'defaultRedirect' => $pluginUrl.$this->page,
        'create' => [
        'redirect' => $pluginUrl.'pageconfig/update/:id',
        'redirectClose' => $pluginUrl.'pageconfig',
        ],
        'update' => [
        'redirect' => $pluginUrl.'pageconfig',
        'redirectClose' => $pluginUrl.'pageconfig',
        ],
        ];
    }

    protected function getPageModelTemplate()
    {
        return "<?php
namespace ".$this->namespace."\Models;

use Model;
use Nitm\Content\Models\PageConfig;

/**
* Model
*/
class %s extends PageConfig
{
}
        ";
    }
}
