<?php

namespace Nitm\Content\Models\ContentView;

use BaoPham\DynamoDb\DynamoDbModel as Model;

/**
* Keeps trace of content views for content using AMazon DynamoDB.
*/
class DynamoContentView extends Model
{
    use \Nitm\Content\Traits\Model;
    use \October\Rain\Support\Traits\Emitter;
    use \October\Rain\Extension\ExtendableTrait;
    use \October\Rain\Database\Traits\DeferredBinding;
    use ContentViewTrait;
    
    public function __construct(array $attributes = [], DynamoDbClientService $dynamoDb = null)
    {
        parent::__construct($attributes, $dynamoDb);
        $this->extendableConstruct();
        $this->extendProperties();
    }
}