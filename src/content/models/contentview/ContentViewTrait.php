<?php

namespace Nitm\Content\Models\ContentView;

trait ContentViewTrait
{
    public function extendProperties()
    {
        $this->fillable = [
        'ip', 'ip_spoofed', 'ip_forwarded', 'host', 'cookie_hash', 'content_type', 'content_id', 'url', 'user', 'created_at',
        'used_key', 'referrer', 'browser', 'status_code', 'request_method', 'api_status', 'duration', 'action',
        'fullurl', 'timepassed',
        ];
        
        if (class_exists('BrowserDetect')) {
            $this->fillable = array_merge($this->fillable, array_keys(\hisorange\BrowserDetect\Parser::$dataSchema));
        }
        $this->fillable = array_map('snake_case', $this->fillable);
        $this->table = $this->table ?? \Config::get('octopus.statistics.model.table');
        $this->guarded = [];
    }
}