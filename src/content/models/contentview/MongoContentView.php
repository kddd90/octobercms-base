<?php

namespace Nitm\Content\Models\ContentView;

use BaoPham\DynamoDb\DynamoDbModel as Model;

class MongoContentView extends Model
{
    use \Nitm\Content\Traits\Model, \October\Rain\Extension\ExtendableTrait;
}