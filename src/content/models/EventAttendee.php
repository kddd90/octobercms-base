<?php

namespace Nitm\Content\Models;

use DB;
use Model;
use Validator;

/**
 * Model.
 */
class EventAttendee extends BaseContent
{
    public $shouldCache = false;
    const STATUS_GOING = 'going';
    const STATUS_MAYBE = 'maybe';
    const STATUS_NOT_GOING = 'not-going';

    /*
     * Validation
     */
    public $rules = [
      'event_id' => 'required|exists:app_content_events,id',
        'status' => 'required|valid_status',
    ];

    public $customMessages = [
      'valid_status' => "What do you mean you're [:status] to the event?",
      'unique_rsvp' => "You've already RSVP'd for this event!",
      'status_required' => 'Are you going, not going or maybe you might go?',
    ];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = true;

    /**
     * @var string The database table used by the model
     */
    public $table = 'app_content_event_attendees';

    public $belongsTo = [
      'attendee' => ['Nitm\Content\Models\User', 'otherKey' => 'id', 'key' => 'attendee_id'],
        'event' => ['Nitm\Content\Models\Event', 'otherKey' => 'id', 'key' => 'event_id'],
   ];

    public $fillable = [
      'event_id', 'attendee_id', 'status', 'exists',
   ];

    public $visible = [
      'id', 'attendee', 'event', 'status', 'created_at',
   ];

    public $with = [
      'attendee',
   ];

    public $touches = ['event'];

    protected static function setupCustomValidators()
    {
        Validator::extend('valid_status', function ($attribute, $value, $parameters, $validator) {
            return in_array($validator->getData()['status'], array_keys(static::getStatusOptions()));
        });
        Validator::replacer('valid_status', function ($message, $attribute, $rule, $parameters) {
            return str_replace(':status', array_pop($parameters), $message);
        });

        Validator::extend('unique_rsvp', function ($attribute, $value, $parameters, $validator) {
            // Get table name from first parameter
             $table = array_shift($parameters);

             // Build the query
             $query = DB::table($table);
            $query->select('id');

             // Add the field conditions
             foreach ($validator->getData() as $field => $value) {
                 $query->where($field, '=', $value);
             }

             // Validation result will be false if any rows match the combination
             return $query->count() == 0;
        });
        Validator::replacer('unique_rsvp', function ($message, $attribute, $rule, $parameters) {
            return str_replace(':event', array_pop($parameters), $message);
        });
    }

   /**
    * Get the art type options.
    *
    * @return array The type ID options
    */
   public static function getStatusOptions()
   {
       return [
          self::STATUS_GOING => 'Going',
          self::STATUS_MAYBE => 'Maybe',
          self::STATUS_NOT_GOING => 'Not Going',
      ];
   }

    public function title()
    {
    }

    /**
     * Custom Find a model by custom keys.
     *
     * @param mixed $id
     * @param array $columns
     *
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Collection|null
     */
    public static function apiFind($id, $columns = ['*'])
    {
        if (is_numeric($id)) {
            return static::newQuery()->where('event_id', '=', $id)->select($columns)->get();
        } else {
            return static::newQuery()->whereIn('event_id', function ($query) use ($id) {
                $query->select('id')->from((new Event())->getTable())->where('username', '=', $id);
            })->select($columns)->get();
        }
    }

    /**
     * Custom Find a model by custom keys.
     *
     * @param array $params
     *
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Collection|null
     */
    public static function apiQuery($params = [], $multiple = false, $query = null)
    {
        $id = array_pull($params, 'id');
        $columns = @$columns ?: '*';
        if (!$id) {
            throw new \InvalidArgumentException('Missing Event ID', 400);
        }
        if (is_numeric($id)) {
            $query = static::query()->where('event_id', '=', $id)->select($columns)->get();
        } elseif (is_string($id)) {
            $query = static::query()->whereIn('event_id', function ($query) use ($id) {
                $query->select('id')->from((new Event())->getTable())->where('username', '=', $id);
            })->select($columns)->get();
        }

        parent::apiQuery([], $multiple, $query);

        return $query;
    }
}
