<?php

namespace Nitm\Content\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateNitmContentCategory extends Migration
{
    public function up()
    {
        if (!Schema::hasTable('nitm_content_categories')) {
            Schema::create('nitm_content_categories', function ($table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->string('title', 128);
                $table->string('slug', 128);
                $table->text('description');
                $table->text('image_path')->nullable();
                $table->integer('author_id')->nullable();
                $table->integer('editor_id')->nullable()->default(0);
                $table->timestamp('updated_at')->nullable();
                $table->timestamp('created_at')->nullable();
                $table->integer('deleter_id')->nullable();
                $table->integer('parent_id')->nullable();
                $table->integer('nest_left')->nullable();
                $table->integer('nest_right')->nullable();
                $table->integer('nest_depth')->nullable();
                $table->softDeletes();
            });
        }
    }

    public function down()
    {
        Schema::dropIfExists('nitm_content_categories');
    }
}
