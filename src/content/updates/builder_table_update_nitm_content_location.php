<?php

namespace Nitm\Content\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNitmContentLocation extends Migration
{
    public function up()
    {
        Schema::table('nitm_content_location', function ($table) {
            $table->string('country_code', 3)->nullable();
            $table->string('state_code', 3)->nullable();
            $table->integer('state_id')->nullable();
            $table->integer('country_id')->nullable();
        });
    }

    public function down()
    {
        Schema::table('nitm_content_location', function ($table) {
            $table->dropColumn('country_code');
            $table->dropColumn('state_code');
            $table->dropColumn('state_id');
            $table->dropColumn('country_id');
        });
    }
}
