<?php

namespace Nitm\Content\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateNitmNitmContentPages extends Migration
{
    public function up()
    {
        if (!Schema::hasTable('nitm_content_pages')) {
            Schema::create('nitm_content_pages', function ($table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->string('page', 64);
                $table->string('modelName', 128);
                $table->string('namespace', 255);
                $table->text('modelClass');
                $table->text('config');
                $table->timestamp('deleted_at')->nullable();
                $table->timestamp('created_at')->nullable();
                $table->integer('author_id')->nullable();
                $table->timestamp('updated_at')->nullable();
                $table->integer('editor_id')->nullable();
            });
        }
    }

    public function down()
    {
        Schema::dropIfExists('nitm_content_pages');
    }
}
