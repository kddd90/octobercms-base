<?php

namespace Nitm\Content\Components;

use Nitm\Content\Models\Follow;
use Nitm\Content\Models\User;

class UserActions extends \Cms\Classes\ComponentBase
{
    protected $_item;
    protected $_currentUser;

    public function componentDetails()
    {
        return [
            'name' => 'User Actions',
            'description' => 'Provides various user actions'
        ];
    }

    public function defineProperties()
    {
        return [
            'type' => [
                'title'             => 'Action Type',
                'description'       => 'The type of action to render',
                'type'              => 'string'
            ],
            'item' => [
                'title'             => 'The item',
                'description'       => 'The item that is affected by this action',
            ],
            'currentUser' => [
                'title'             => 'The current user',
                'description'       => 'The current user performing the action'
            ],
        ];
    }

    public function currentUser() {
        if(!isset($this->_currentUser)) {
            $item = $this->property('currentUser') ? $this->property('currentUser') : \Auth::getUser();
            if(is_array($item)) {
                $item = new \Nitm\Content\Models\RelatedUser($item);
            }
            $this->_currentUser = $item;
        }
        return $this->_currentUser;
    }

    public function item() {
        if(!isset($this->_user)) {
            $item = $this->property('item') ? $this->property('item') : User::apiFind(input('id'));
            if(is_array($item)) {
                $item = new \Nitm\Content\Models\RelatedUser($item);
            }
            $this->_item = $item;
        }
        return $this->_item;
    }

    public function type() {
        return $this->property('type') ?: 'follow';
    }

    public function uniqueId() {
        switch($this->type()) {
            case 'follow':
            $type = $this->type();
            if($this->currentUser()) {
                $type = $this->currentUser()->isFollowing($this->item()->id) ? 'unfollow' : 'follow';
            }
            return $type.'-'.$this->item()->id;
            break;

            default:
            return uniqid();
            break;
        }
    }
    
    public function onFollow() {
        if(!$this->currentUser()) {
            \Flash::error("You need to be logged in to do this");
            return false;
        }
        $follow = Follow::where([
            'follower_id' => $this->currentUser()->id,
            'followee_id' => $this->item()->id
        ])->first();
        if($follow) {
            $follow->restore();
        } else {
            $follow = Follow::create([
                'follower_id' => $this->currentUser()->id,
                'followee_id' => $this->item()->id
            ]);
        }
        \Flash::success("You're now following ".$this->item()->title());
        return [
            $this->getPartialId('unfollow') => $this->renderPartial('@'.$this->type().'.htm'),
            $this->getPartialId('follow') => $this->renderPartial('@'.$this->type().'.htm')
        ];
    }

    protected function getPartialId($action) {
        
        return '#'.implode('-', [
            'user-action',
            $action,
            $this->item() ? $this->item()->id : uniqid()
        ]);
    }
    
    public function onUnFollow() {
        if(!$this->currentUser()) {
            \Flash::error("You need to be logged in to do this");
            return false;
        }
        $follow = Follow::where([
            'follower_id' => $this->currentUser()->id,
            'followee_id' => $this->item()->id
        ])->first();
        if($follow) {
            $follow->delete();
            \Flash::success("You just unfollowed ".$this->item()->title());
            return [
                $this->getPartialId('unfollow') => $this->renderPartial('@'.$this->type().'.htm'),
                $this->getPartialId('follow') => $this->renderPartial('@'.$this->type().'.htm')
            ];
        } else {
            \Flash::error("Couldn't unfollow");
        }
    }
}
?>