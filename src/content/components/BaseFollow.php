<?php

namespace Nitm\Content\Components;

class BaseFollow extends Paginated
{
    protected $users;

    public function componentDetails()
    {
        return [
            'name' => 'Follow',
            'description' => 'Displays a collection of user items.'
        ];
    }
}
?>