<?php

namespace Nitm\Content\Components;

class Followers extends BaseFollow
{
    public function componentDetails()
    {
        return [
            'name' => 'Followers',
            'description' => "Displays a collection of the user's followers."
        ];
    }

    /**
     * Get the users that are following the profile
     */
    public function users()
    {
        if(!isset($this->users)) {
            $query = $this->profile()->followers();
            $query->orderBy(array_get(input(), 'sort', 'id'), array_get(input(), 'order', 'desc'));
            $this->items = $query
                ->paginate($this->property('maxItems'), ['*'], 'page', input('page', 1));;
            $this->users = $this->items->getCollection();
        }
        return $this->users;
    }
}
?>