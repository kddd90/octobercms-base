<?php

namespace Nitm\Content\Components;

class Pagination extends \Cms\Classes\ComponentBase
{
    protected $users;
    protected $profile;

    public function componentDetails()
    {
        return [
            'name' => 'Pagination',
            'description' => 'Displays a list of pages'
        ];
    }

    public function defineProperties()
    {
        return [
            'component' => [
                'title'             => 'Component',
                'description'       => 'The component',
            ],
            'route' => [
                'title'             => 'Route',
                'description'       => 'The route to run when more is clicked',
                'default'           => null,
            ],
            'action' => [
                'title'             => 'Action',
                'description'       => 'The action to run when more is clicked',
                'default'           => 'onMore',
            ],
            'text' => [
                'title'             => 'Text',
                'description'       => 'The text for the button',
                'default'           => 'Show more',
            ],
            'pageParam' => [
                'title'             => 'Text',
                'description'       => 'The parameter for the page value',
                'default'           => 'page',
            ],
            'buttonClass' => [
                'title'             => 'Button Class',
                'description'       => 'The css class for the button',
                'default'           => 'btn btn--lazy-load btn-info',
            ],
            'buttonSvg' => [
                'title'             => 'Button SVG',
                'description'       => 'The svg html for the button',
                'default'           => '<svg class="icon icon--lazy-load"><use xlink:href="#icon--lazy-load"></use></svg>',
            ],
        ];
    }

    public function component()
    {
        return $this->property('component');
    }

    public function action()
    {
        return $this->property('action');
    }

    public function text()
    {
        return $this->property('text');
    }

    public function buttonClass()
    {
        return $this->property('buttonClass');
    }

    public function buttonSvg()
    {
        return $this->property('buttonSvg');
    }

    public function getRoute()
    {
        return $this->property('route') ?: $this->page->baseFileName;
    }
}
