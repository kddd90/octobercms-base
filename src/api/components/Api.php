<?php

namespace Nitm\Api\Components;

use Cms\Classes\Controller;
use Nitm\Api\Classes\Trivet;

class Api extends BaseApiComponent
{
    protected static function getControllerClass()
    {
        return \Nitm\Api\Controllers\ApiController::class;
    }

    public function componentDetails()
    {
        return [
            'name' => 'nitm.api::lang.components.api.name',
            'description' => 'nitm.api::lang.components.api.desc',
        ];
    }

    public function defineProperties()
    {
        return [
            'apiStatus' => [
                'title' => 'nitm.api::lang.settings.api_status.title',
                'description' => 'nitm.api::lang.settings.api_status.component_detail',
                'type' => 'dropdown',
                'default' => 'on',
                'options' => [
                    'on' => 'nitm.api::lang.settings.api_status.on',
                    'off' => 'nitm.api::lang.settings.api_status.off',
                ],
            ],
        ];
    }

    /**
     * Starter method of the component.
     *
     * @return string
     */
    public function onRun()
    {
        /*
          * Do some common checks before running the operation
          */
         $methodToRun = $this->beforeRun();
        if ($methodToRun instanceof \Illuminate\Http\JsonResponse) {
            return $methodToRun;
        }

        return $this->getResult([$this, $methodToRun]);
    }

    protected function beforeRun()
    {
        $beforeRun = parent::beforeRun();
        if ($beforeRun instanceof \Illuminate\Http\JsonResponse) {
            return $beforeRun;
        }

        /*
         * Check if user is authorized for this operation?
         */
        $this->helper->checkIfAuthCorrect($this->requiresAuth());

        /*
         * Assign index key name to variable
         * for readOneData(), updateData(), deleteData()
         */
        $this->controller->where_index_key = Trivet::getInputs('key') ?: 'id';

        /*
         * Check if request "req" parameter is ok
         */
        $this->checkIfRequestOk();

        /*
         * Check if created mapping relation is "read only"
         */
        $this->checkIfReadOnly();

         /*
          * Check "do" parameter and determine the Operation which needs to run
          */

        return $this->helper->checkIfCRUD($this->controller->operations);
    }
}
