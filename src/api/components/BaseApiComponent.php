<?php

namespace Nitm\Api\Components;

use Route;
use Cms\Classes\ComponentBase;
use Cms\Classes\Page;
use Cms\Classes\Controller;
use Cms\Classes\CodeParser;
use Nitm\Api\Classes\Rest;
use Nitm\Api\Helpers\ApiHelper;

abstract class BaseApiComponent extends ComponentBase
{
    /**
    * Holds REST Class instance.
    */
   public $rest;

   /**
    * Holds Helper Component instance.
    */
   public $helper;

    public $controller;

   /**
    * Define methods for operations
    * 'do parameter' => 'method name'.
    */
   public $operations = [];

   /**
    * Holds all request parameters for all relations.
    */
   public $request_parameters = [];

   /**
    * Has the necessary per run checks been performed?
    *
    * @var bool
    */
   protected $_hasPerformedChecks;

    public function __construct()
    {
        parent::__construct();
        $this->pageObj = $this->spoofPageCode();
    }

    public function __call($method, $parameters)
    {
        if (strtolower($method) == 'callaction') {
            $this->tryResult(function () use ($parameters) {
                if (!$this->_hasPerformedChecks) {
                    $this->beforeRun();
                }
            }, true);
            if ($this->controller->hasErrors()) {
                return $this->controller->getErrorResponse();
            } else {
                return call_user_func_array([$this, $parameters[0]], $parameters[1]);
            }
        } else {
            return parent::__call($method, $parameters);
        }
    }

    protected static function getControllerClass()
    {
        throw new \Exception('You need to specify the controller class!');
    }

    /**
     * Returns a defined property or parameter value.
     *
     * @param $name - The property or parameter name to look for
     * @param $default - A default value to return if no value found
     *
     * @return string
     */
    public function propertyOrParam($name, $default = null)
    {
        $value = $this->property($name, $default);

        if (substr($name, 0, 1) == ':') {
            return $this->param(str_replace(':', '', $name), $default);
        }

        return $value;
    }

    /**
     * Perform some standard operations before runing the request
     * Will throw an eror if there is an issue otherwise will allow the request to go through.
     */
    protected function beforeRun()
    {
        /* Get an instance of Rest Class */
        $this->rest = Rest::instance();

        if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
            $origins = (array) config('cors.allowedOrigins');
            $origin = in_array(array_get($_SERVER, 'HTTP_ORIGIN', ''), $origins) ? $_SERVER['HTTP_ORIGIN'] : $origins[0];
            //Somehow CORS broke and this needs to be done here
            header('Access-Control-Allow-Methods: '.implode(',', (array) config('cors.allowedMethods')));
            $response = $this->rest->response(200, true);

            return $response;
        }

        /* Instantiate the ApiHelper */
        $this->helper = new ApiHelper($this);

        /*
         * Check if API not activated, or passived in the page
         */
        $this->helper->checkIfApiActivated($this->propertyOrParam('apiStatus'));

        /*
         * Check if API shutdown schedule is now (started)
         */
        $this->helper->checkIfScheduleStarted();

        /*
         * and then, check IP Blacklist
         */
        $this->helper->checkIfIpBlocked();

       /* Check if mandatory fields for this request are ok */
        $this->checkIfParamsOk();

        $this->_hasPerformedChecks = true;
    }

    /**
     * Get the parameters for the route.
     *
     * @param string     $req The entity type of request [user, art,...]
     * @param string|int $id  The specific id we need to find
     * @param string     $do  The action to run
     * @param string     $key The primary key for the record we are searching for
     *
     * @return array The route parameters
     */
    public function getParameters($req, $id = null, $key = 'id')
    {
        $parameters = [
              'req' => $req,
          ];
        if ($id != null) {
            $parameters = array_merge($parameters, [
                 'id' => $id,
                 'key' => $key,
              ]);
        }

        return $parameters;
    }

    /**
     * Create a blank page for the component.
     *
     * @param array $routerParameters THe parameters for the route
     * @param array $pageSettings     The settings for the generated page
     *
     * @return CodeBase The code base page object
     */
    public function spoofPageCode()
    {
        if (!isset($this->page)) {
            $this->page = new Page();
        }
        if (!isset($this->controller)) {
            $controllerClass = static::getControllerClass();
            $this->controller = new $controllerClass();
        }
        $parser = new CodeParser($this->page);
        $pageObj = $parser->source($this->page, 'no-layout', $this->controller);

        return $pageObj;
    }
}
