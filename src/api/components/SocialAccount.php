<?php

namespace Nitm\Api\Components;

use Lang;
use Response;

class SocialAccount extends BaseApiComponent
{
    protected static function getControllerClass()
    {
        return \Nitm\Api\Controllers\SocialAccountController::class;
    }

    public function defineProperties()
    {
        $properties = array(
            'redirect' => [
                'title' => 'rainlab.user::lang.account.redirect_to',
                'description' => 'rainlab.user::lang.account.redirect_to_desc',
                'type' => 'dropdown',
                'default' => '',
            ],
        );

        return $properties;
    }

    public function componentDetails()
    {
        return [
            'name' => 'nitm.api::lang.components.social.name',
            'description' => 'nitm.api::lang.components.social.desc',
        ];
    }

    /**
     * Starter method of the component.
     *
     * @return string
     */
    public function onRun()
    {
        /*
          * Do some common checks before running the operation
          */
         $beforeRun = $this->beforeRun();
        if ($beforeRun instanceof \Illuminate\Http\JsonResponse) {
            return $beforeRun;
        }

        /*
         * If API is activated, IP is good, Auth is good,
         * Start the API, build response, make output with proper headers
         */
        return $this->getResult([$this, 'connectAccount']);
    }

    protected function beforeRun()
    {
        parent::beforeRun();
    }

    /**
     * Check if "req" parameter matches with any relation or table, else stop.
     */
    protected function checkIfRequestOk()
    {
        return true;
    }
}
