<?php
/**
 * @SWG\Tag(
 *   name="art",
 *   description="Octopus Artworks Art",
 * )
 * @SWG\Tag(
 *   name="user",
 *   description="Operations for Octopus Artworks users"
 * )
 */
