<?php
/**
 * @SWG\Tag(
 *   name="art",
 *   description="Operations for art",
 * )
 * @SWG\Tag(
 *   name="user",
 *   description="Operations for users"
 * )
 * @SWG\Tag(
 *   name="event",
 *   description="Operations for events"
 * )
 * @SWG\Tag(
 *   name="rsvp",
 *   description="Operations for event rsvps"
 * )
 * @SWG\Tag(
 *   name="auth",
 *   description="Operations for authentication/account operations"
 * )
 * @SWG\Tag(
 *   name="account",
 *   description="Operations for accounts"
 * )
 * @SWG\Tag(
 *   name="follow",
 *   description="Operations for following content"
 * )
 * @SWG\Tag(
 *   name="favorite",
 *   description="Operations for favoriting content"
 * )
 * @SWG\Tag(
 *   name="feed",
 *   description="Operations for activity feed"
 * )
 * @SWG\Tag(
 *   name="category",
 *   description="Operations for categories"
 * )
 * @SWG\Tag(
 *   name="profile",
 *   description="Operations for user profiles"
 * )
 * @SWG\Tag(
 *   name="sellArtConfig",
 *   description="Sell Art page configuration"
 * )
 * @SWG\Tag(
 *   name="homeConfig",
 *   description="Home page configuration"
 * )
 * @SWG\Tag(
 *   name="artConfig",
 *   description="Art page configuration"
 * )
 * @SWG\Tag(
 *   name="artistsConfig",
 *   description="Artists page configuration"
 * )
 * @SWG\Tag(
 *   name="eventConfig",
 *   description="Events page configuration"
 * )
 * @SWG\Tag(
 *   name="blogConfig",
 *   description="Blog page configuration"
 * )
 * @SWG\Tag(
 *   name="createEventConfig",
 *   description="Create event page configuration"
 * )
 * @SWG\Tag(
 *   name="createBlogConfig",
 *   description="Create blog post page configuration"
 * )
 */
