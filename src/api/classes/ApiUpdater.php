<?php

namespace Nitm\Api\Classes;

use Carbon\Carbon;
use DB;
use Schema;
use Nitm\Api\Models\Configs as RestfulConfig;
use Illuminate\Database\QueryException;
use October\Rain\Exception\ApplicationException;

class ApiUpdater
{
    use \Nitm\Api\Traits\ApiTrait;
    /**
     * Holds response data.
     */
    public $data;

    /**
     * Holds REST Class instance.
     */
    public $rest;

    /**
     * Holds columns for matching with database.
     */
    public $columns_to_operate = [];

    /**
     * Holds all relations.
     */
    public $all_relations = [];

    /**
     * Holds request parameters.
     */
    public $request_parameters = [];

    /**
     * Holds all fields.
     */
    public $fields = [];

    /**
     * Holds index key.
     */
    public $where_index_key;

    /**
     * Holds index key value.
     */
    public $update_by_index_key;

    /**
     * Starter method of the component.
     */
    public function __construct()
    {
        $this->rest = Rest::instance();
        $this->type = Trivet::getInputs('req');
    }

    /**
     * Get all relations data.
     *
     * @param $fields
     * @param $all_relations
     * @param $request_parameters
     * @param $where_index_key
     */
    public function passData($fields, $all_relations, $request_parameters, $where_index_key)
    {
        $this->fields = $fields;
        $this->all_relations = $all_relations;
        $this->request_parameters = $request_parameters;

        /* If index key sent as parameter, hold for next queries */
        $this->update_by_index_key = Trivet::getInputs($where_index_key) != null ? Trivet::getInputs($where_index_key) : 0;
        $this->where_index_key = $where_index_key;
    }

    /**
     * Build column fields for insert.
     */
    public function buildColumnFields()
    {
        /* Build column fields for insert */
        foreach (Trivet::getInputs() as $key => $value) {
            if (!in_array($key, $this->fields['mandatory']) && !in_array($key, $this->fields['purge'])) {
                $this->columns_to_operate[$key] = $value;
            }
        }
    }

    /**
     * Check & fetch relation based data before direct table access control.
     */
    public function makeRelationBasedUpdate()
    {
        /* Fetch Table columns for matching with parameters */
        $mapping = $this->all_relations[$this->type];
        if (class_exists(($class = $mapping->relatedtable))) {
            $class::setForApi(true);
            $model = $class::apiFind($this->update_by_index_key, ['hashedId' => true]);
            if ($model) {
                if (!$model->can('update')) {
                    throw new ApplicationException(trans('nitm.content::lang.errors.no_permission'), 403);
                }
                $this->saveModel($model);
            } else {
                throw new \Exception(trans('nitm.api::lang.responses.no_data_all'), 400);
            }

            return;
        } else {
            /* Fetch Table columns for matching with parameters */
           $dbFields = Trivet::getDbFields();
            $fieldsInDb = [];

            foreach ($dbFields as $dbField) {
                if ($dbField->TABLE_NAME == $this->all_relations[$this->type]->relatedtable) {
                    $fieldsInDb[$dbField->COLUMN_NAME] = $dbField->COLUMN_NAME;
                }
            }

           /* Check for if any columns to operate defined */
           if (empty($this->columns_to_operate)) {
               Trivet::addApiLog(0, 400);
               throw new \Exception(trans('nitm.api::lang.responses.no_column_to_operate'), 400);
           }

           /* Probably "updated_at" not sent by parameters.. */
           $timeNow = date('Y-m-d H:i:s');

            if (!isset($this->columns_to_operate['updated_at'])) {
                $this->columns_to_operate['updated_at'] = $timeNow;
            }

            foreach ($this->columns_to_operate as $key => $value) {
                if (!in_array($key, $fieldsInDb)) {
                    unset($this->columns_to_operate[$key]);
                }
            }

            try {
                $this->data = DB::table($this->all_relations[$this->type]->relatedtable)
                   ->where($this->where_index_key, $this->update_by_index_key)
                   ->update($this->columns_to_operate);
            } catch (QueryException $e) {
                Trivet::addApiLog(0, 400);
                throw new \Exception(trans('nitm.api::lang.responses.update_failed', [
                   'indexkey' => '#'.$this->where_index_key.': '.$this->update_by_index_key,
               ]), 400);
            } catch (\Exception $e) {
                Trivet::addApiLog(0, 400);
                throw new \Exception(trans('nitm.api::lang.responses.update_failed', [
                   'indexkey' => '#'.$this->where_index_key.': '.$this->update_by_index_key,
               ]), 400);
            }
        }
    }

    /**
     * Check if direct table access allowed.
     */
    public function makeDirectTableUpdate()
    {
        /* Check if direct table access allowed */
        if (RestfulConfig::get('direct_table_output')) {
            /* If requested table exists */
            if (Schema::hasTable($this->type)) {
                /* Fetch Table columns for matching with parameters */
                $dbFields = Trivet::getDbFields();
                $fieldsInDb = [];

                foreach ($dbFields as $dbField) {
                    if ($dbField->TABLE_NAME == $this->type) {
                        $fieldsInDb[$dbField->COLUMN_NAME] = $dbField->COLUMN_NAME;
                    }
                }

                /* Check for if any columns to operate defined */
                if (empty($this->columns_to_operate)) {
                    Trivet::addApiLog(0, 400);
                    throw new \Exception(trans('nitm.api::lang.responses.no_column_to_operate'), 400);
                }

                /* Probably "updated_at" not sent by parameters.. */
                $timeNow = Carbon::now();

                if (!isset($this->columns_to_operate['updated_at'])) {
                    $this->columns_to_operate['updated_at'] = $timeNow;
                }

                foreach ($this->columns_to_operate as $key => $value) {
                    if (!in_array($key, $fieldsInDb)) {
                        unset($this->columns_to_operate[$key]);
                    }
                }

                /* unset handler key selector */
                unset($this->columns_to_operate[Trivet::getInputs('key')]);

                /* Try.. */
                try {
                    $this->data = DB::table($this->type)
                        ->where($this->where_index_key, $this->update_by_index_key)
                        ->update($this->columns_to_operate);
                } catch (QueryException $e) {
                    Trivet::addApiLog(0, 400);
                    throw new \Exception(trans('nitm.api::lang.responses.update_failed', [
                        'indexkey' => '#'.$this->where_index_key.': '.$this->update_by_index_key,
                    ]), 400);
                } catch (\Exception $e) {
                    Trivet::addApiLog(0, 400);
                    throw new \Exception(trans('nitm.api::lang.responses.update_failed', [
                        'indexkey' => '#'.$this->where_index_key.': '.$this->update_by_index_key,
                    ]), 400);
                }
            }
        } else {
            /* Check if request matches with any relation */
            if (!in_array($this->type, $this->request_parameters)) {
                Trivet::addApiLog(0, 400);
                throw new \Exception(trans('nitm.api::lang.responses.req_mismatch'), 400);
            }
        }
    }

    /**
     * Check for if update failed.
     */
    public function lastControl()
    {
        /* If update failed */
        if (!$this->data) {
            Trivet::addApiLog(0, 400);
            throw new \Exception(trans('nitm.api::lang.responses.update_failed', [
                'indexkey' => '#'.$this->where_index_key.': '.$this->update_by_index_key,
            ]), 400);
        }

        return [
           'id' => $this->update_by_index_key,
           'message' => trans('nitm.api::lang.responses.update_ok', [
               'indexkey' => '#'.$this->where_index_key.': '.$this->update_by_index_key,
           ])
        ];
    }
}
