<?php

namespace Nitm\Api\Classes;

use Illuminate\Support\Arr;
use October\Rain\Support\Traits\Singleton;
use Request;
use Nitm\Api\Models\Configs as RestfulConfig;

class Rest
{
    use Singleton;

    public $isMultiple;

    /**
     * The mode tha api will operat in.
     *
     * @var string in ['jsonapi', 'rest', json', 'xml']
     */
    public $apiMode = 'restApi';

    /**
     * The type of the data being returned.
     *
     * @var string
     */
    public $dataType;
    public $request = [];
    public $responseType = [
        'xml' => 'application/xml',
        'json' => 'application/json',
    ];

    protected $pagination = [
      'count' => 1,
      'last' => 1,
      'next' => 1,
      'previous' => 1,
      'current' => 1,
   ];

    private $_statusCode = 200;
    private $_response;

    public function init()
    {
        return $this->inputs();
    }

    private function getStatusMessage()
    {
        $status = $this->statuses();

        return array_get($status, $this->_statusCode, $status[500]);
    }

    protected function statuses()
    {
        return [
          /* Default for CRUD Operations */
          200 => 'OK',
          201 => 'Created',
          204 => 'No Content',
          404 => 'Not Found',
          406 => 'Not Acceptable',

          /* 1xx Informational */
          100 => 'Continue',
          101 => 'Switching Protocols',
          102 => 'Processing',

          /* 2xx Success */
          200 => 'OK',
          201 => 'Created',
          202 => 'Accepted',
          203 => 'Non-Authoritative Information',
          204 => 'No Content',
          205 => 'Reset Content',
          206 => 'Partial Content',
          207 => 'Multi-Status',
          208 => 'Already Reported',
          226 => 'IM Used',

          /* 3xx Redirections */
          300 => 'Multiple Choices',
          301 => 'Moved Permanently',
          302 => 'Found',
          303 => 'See Other',
          304 => 'Not Modified',
          305 => 'Use Proxy',
          306 => 'Switch Proxy',
          307 => 'Temporary Redirect',
          308 => 'Permanent Redirect',

          /* 4xx Client Errors */
          400 => 'Bad Request',
          401 => 'Unauthorized',
          402 => 'Payment Required',
          403 => 'Forbidden',
          404 => 'Not Found',
          405 => 'Method Not Allowed',
          406 => 'Not Acceptable',
          407 => 'Proxy Authentication Required',
          408 => 'Request Timeout',
          409 => 'Conflict',
          410 => 'Gone',
          411 => 'Length Required',
          412 => 'Precondition Failed',
          413 => 'Request Entity Too Large',
          414 => 'Request-URI Too Long',
          415 => 'Unsupported Media Type',
          416 => 'Requested Range Not Satisfiable',
          417 => 'Expectation Failed',
          418 => 'I\'m a teapot',
          419 => 'Authentication Timeout',
          420 => 'Method Failure',
          421 => 'Misdirected Request',
          422 => 'Unprocessable Entity',
          423 => 'Locked',
          424 => 'Failed Dependency',
          426 => 'Upgrade Required',
          428 => 'Precondition Required',
          429 => 'Too Many Requests',
          431 => 'Request Header Fields Too Large',
          440 => 'Login Timeout',
          444 => 'No Response',
          449 => 'Retry With',
          450 => 'Blocked by Windows Parental Controls',
          451 => 'Unavailable For Legal Reasons',
          494 => 'Request Header Too Large',
          495 => 'Cert Error',
          496 => 'No Cert',
          497 => 'HTTP to HTTPS',
          498 => 'Token expired/invalid',
          499 => 'Token required',

          /* 5xx Server Errors */
          500 => 'Internal Server Error',
          501 => 'Not Implemented',
          502 => 'Bad Gateway',
          503 => 'Service Unavailable',
          504 => 'Gateway Timeout',
          505 => 'HTTP Version Not Supported',
          506 => 'Variant Also Negotiates',
          507 => 'Insufficient Storage',
          508 => 'Loop Detected',
          509 => 'Bandwidth Limit Exceeded',
          510 => 'Not Extended',
          511 => 'Network Authentication Required',
          598 => 'Network read timeout error',
          599 => 'Network connect timeout error',
      ];
    }

    private function setHeaders()
    {
        $this->_response = response();
    }

    private function inputs()
    {
        switch (Request::server('REQUEST_METHOD')) {
            /* TODO: Re-code for Delete and Put methods */
            case 'POST':
            case 'GET':
            case 'DELETE':
                $this->request = Trivet::getInputs();
                break;
            case 'PUT':
                parse_str(file_get_contents('php://input'), $this->request);
                break;
            default:
                $this->response(405, trans('Nitm.restful::lang.responses.method_mismatch'));
                break;
        }

        /* Define default output */
        $this->request['type'] = (isset($this->request['type']) ? $this->request['type'] : RestfulConfig::get('output_style'));

        return true;
    }

    /**
     * Output as JSON or XML.
     *
     * @param $models
     * @param null $requestTable
     *
     * @return string
     */
    public function makeOutput($models, $requestTable = null)
    {
        $this->setHeaders();

        if ($models instanceof \Illuminate\Http\RedirectResponse) {
            return $models;
        } else {
            if ($models instanceof \Illuminate\Http\JsonResponse
           || $models instanceof \Illuminate\Http\XmlResponse) {
                $response = $models;
            } else {
                if (@$this->request['type'] == 'xml') {
                    $response = $this->xml($models, $requestTable);
                } else {
                    $response = $this->json($models, $requestTable);
                }
            }
            $response = app('Barryvdh\Cors\Stack\CorsService')->addActualRequestHeaders($response, request());

            return $response;
        }
    }

    /**
     * Encode array into JSON
     * Added "JSON_UNESCAPED_UNICODE", so you need PHP ver. > 5.4.0.
     *
     * @param $models
     *
     * @return string
     */
    public function json($models, $type)
    {
        $jsonData = '';

         /* Decide if response error or normal data */
         // if(isset($models['errors']))
         // $responseData = (is_array($models['detail']) ? $models['detail'] : $models);
         /* If "JSON_UNESCAPED_UNICODE" prefered.. */
         switch ($this->apiMode) {
            case 'jsonApi':
            $responseData = [
               'data' => $this->formatForJsonApi($models, $type),
            ];
            break;

            default:
            $type = $this->isMultiple ? str_plural($type) : str_singular($type);
            $responseData = [
               $type => $this->formatForRestApi($models, $type),
            ];
            break;
         }
        if ($this->isMultiple) {
            $responseData['meta'] = [
               'pagination' => array_pull($responseData, $type.'.pagination', $this->pagination),
            ];
        }

        return $this->_response->json($responseData);
    }

    protected function serializeModels($models)
    {
        foreach ($models as $idx => $model) {
            $model = $this->serialize($model);
            $models[$idx] = $model;
        }

        return $models;
    }

    protected function serializeModel($model)
    {
        if (is_array($model)) {
            return $model;
        } else {
            $type = \Nitm\Content\Classes\ModelHelper::getIs($model);
            $countAttrs = preg_grep("/\_count/", (array) $model->visible);
            $model->setForAPi(true);
            $relations = $model->relationsToArray();
            $attributes = $model->attributesToArray(true);
            //Get count values for counted relations
            if (is_array($countAttrs) && count($countAttrs)) {
                foreach ($countAttrs as $countAttr) {
                    if (in_array($countAttr, $model->with)) {
                        try {
                            if ($model->{$countAttr}) {
                                $relations[$countAttr] = $model->{$countAttr}->count();
                            } else {
                                $relations[$countAttr] = 0;
                            }
                        } catch (\Exception $e) {
                            if ($model->{$countAttr}) {
                                $relations[$countAttr] = $model->{$countAttr};
                            } else {
                                $relations[$countAttr] = 0;
                            }
                        }
                    }
                }
            }
            $model = array_merge($attributes, [
               '_relations' => $relations,
            ]);
            // $model['type'] = str_singular($type);

            return $model;
        }
    }

   /**
    * Serialize the data.
    *
    * @param  mixed $data The data to be serialized
    *
    * @return mized        The serialized data for the model
    */
   public function serialize($data)
   {
       if ($data instanceof \Model) {
           return $this->serializeModel($data);
      //  } elseif (is_array($data) && count($data) == 1 && is_object(current($data))) {
         //   return $this->serializeModel(current($data));
       } elseif (is_array($data) && !Arr::isAssoc($data)) {
           return $this->serializeModels($data);
       } elseif ($data instanceof \October\Rain\Database\Collection) {
           return $data->toArray(true);
       } elseif ($data instanceof \Illuminate\Pagination\LengthAwarePaginator) {
           $this->setPaginationFromCollection($data);

           return $data->getCollection()->toArray(true);
       }

       return $data;
   }

    public function getPagination()
    {
        return $this->pagination;
    }

    public static function getPaginationFromCollection($collection)
    {
        return \Nitm\Content\Classes\ModelHelper::getPaginationFromCollection($collection);
    }

    public function setPaginationFromCollection($collection)
    {
        $this->pagination = static::getPaginationFromCollection($collection);
    }

    public function extractRelations($relations, $keys = null)
    {
        if (is_array($relations)) {
            foreach ($relations as $relation) {
                $relations[$idx] = is_null($keys) ? $this->serialize($relation) : array_only($relation, $keys);
            }

            return $relations;
        } else {
            return $relations;
        }
    }

    /**
     * Serialize the data according to the JSONApi specification: http://jsonapi.org/format/.
     *
     * @param mixed  $models The models | model
     * @param string $type   The type of the data
     *
     * @return aray the data
     */
    protected function formatForJsonApi($models, $type)
    {
        if ($this->_statusCode != 200) {
            $models = [
             'errors' => $this->getErrors($models),
         ];
        } else {
            $prepareModel = function ($model, $included = []) use ($type) {
                $relationships = empty($included) ? [] : array_diff_key($model, $included);
                if (array_get($model, 'id')) {
                    return [
                       'id' => array_pull($model, 'id'),
                     //   'type' => str_singular($type ?: array_pull($model, 'type')),
                       'attributes' => array_except($model, ['id']),
                       'relationships' => $relationships,
                    ];
                } else {
                    return null;
                }
            };
            if (is_array($models) || $models instanceof \October\Rain\Database\Collection) {
                if ($models instanceof \October\Rain\Database\Collection) {
                    $models = $models->all();
                }
                $result = $this->serialize($models);

                if (Arr::isAssoc($result)) {
                    return $result;
                }

                $included = [];
                foreach ($models as $idx => $model) {
                    $models[$idx] = [
                     'id' => array_pull($result[$idx], 'id'),
                     // 'type' => array_pull($result[$idx], 'type'),
                     'attributes' => array_except($result[$idx], ['id', '_relations']),
                     // Only keep the relationships that are unique to this model
                     // 'relationships' => array_diff_key($relationships, $included),
                     'relationships' => array_get($result[$idx], '_relations', []),
                  ];
                }

                return [
                  'data' => $models,
                  'meta' => [
                     'count' => count($models),
                  ],
                  // 'included' => $relationships,
               ];
            } elseif (is_object($models)) {
                $model = $this->serialize($models);
                if ($model instanceof \Illuminate\Http\JsonResponse
                || $model instanceof \Illuminate\Http\XmlResponse
                || $model instanceof \Illuminate\Http\RedirectResponse) {
                    $models = $model;
                } else {
                    $models = [
                       'data' => [
                          'id' => array_pull($model, 'id'),
                        //   'type' => array_pull($model, 'type'),
                          'attributes' => array_except($model, ['id', '_relations']),
                          'relationships' => array_get($result[$idx], '_relations', []),
                       ],
                     ];
                }
            }
        }

        return $models;
    }

    /**
     * Serialize the data according to the JSONApi specification: http://jsonapi.org/format/.
     *
     * @param mixed  $models The models | model
     * @param string $type   The type of the data
     *
     * @return aray the data
     */
    protected function formatForRestApi($models, $type)
    {
        if ($this->_statusCode != 200) {
            $models = [
             'errors' => $this->getErrors($models),
         ];
        } else {
            if ($models instanceof \Illuminate\Http\JsonResponse
              || $models instanceof \Illuminate\Http\XmlResponse
              || $models instanceof \Illuminate\Http\RedirectResponse) {
                return $models;
            } elseif (is_array($models) && !Arr::isAssoc($models)) {
                $models = $this->serialize($models);
                $relations = [];
                foreach ($models as $idx => $model) {
                    $model = is_object($model) ? $model->toArray(true) : (array) $model;
                    $models[$idx] = array_merge($model, array_pull($model, '_relations', []));
                    unset($models[$idx]['_relations']);
                }

                return $models;
            } elseif (is_object($models) && in_array(get_class($models), [
               'October\Rain\Database\Collection',
               'Illuminate\Pagination\LengthAwarePaginator',
            ])) {
                $models = $this->serialize($models);
            } elseif (is_object($models) && $models instanceof \Illuminate\Database\Eloquent\Model) {
                $hasType = isset($model->type);
                $model = $this->serialize($models);
                $models = array_merge($model, array_pull($model, '_relations', []));
                unset($models['_relations']);
            }
        }

        return $models;
    }

    /**
     * Serialize the data according to the JSONApi specification: http://jsonapi.org/format/.
     *
     * @param mixed  $models The models | model
     * @param string $type   The type of the data
     *
     * @return aray the data
     */
    protected function formatForApi($models, $type)
    {
        if ($this->_statusCode != 200) {
            $models = [
             'errors' => $this->getErrors($models),
         ];
        } else {
            if (is_array($models)) {
                $relationIds = [];
                foreach ($models as $model) {
                    foreach ($model->getRelations() as $name => $relation) {
                        if (is_array($relation)) {
                            $relationIds[$name] = isset($relations[$name]) ? array_unique(array_merge($relations[$name], [$relation['id']])) : [$relation['id']];
                        }
                    }
                }

                return array_merge([
               str_plural($type) => $this->serialize($models),
               $relationIds,
            ]);
            } elseif (is_object($models)) {
                $models = $this->serialize($models);
            }
        }

        return $models;
    }

    /**
     * Make XML based output.
     *
     * TODO: This method will be re-code (with XML attributes support)
     *
     * @param $models
     * @param null $requestTable
     *
     * @return string
     */
    public function xml($models, $requestTable = null)
    {
        /* Add multiple s at the end of the request name to be nicer xml design */
        $requestTable = (substr($requestTable, -1) == 's' ? $requestTable : $requestTable.'s');
        $xmlData = '<?xml version="1.0" encoding="'.RestfulConfig::get('charsets').'"?>
<'.$requestTable.'>';

        /* Check if response is Error or normal data */
        $foreachData = (is_array(array_get($models, 'detail', null)) ? $models['detail'] : $models);
        foreach ($foreachData as $dataKey => $dataValue) {
            /* If multiple results in array */
            if (is_array($dataValue) || is_object($dataValue)) {
                $eachName = substr($requestTable, 0, -1);
                $xmlData .= '
    <'.$eachName.'>';
                foreach ($dataValue as $prodKey => $prodValue) {
                    /* If CDATA adding prefered */
                    if (RestfulConfig::get('xml_cdata')) {
                        $XMLcontent = '<![CDATA['.$prodValue.']]>';
                    } else {
                        $XMLcontent = $prodValue;
                    }
                    $xmlData .= '
        <'.$prodKey.'>'.$XMLcontent.'</'.$prodKey.'>';
                }
                $xmlData .= '
    </'.$eachName.'>';
            } else {
                /* If Only one result */
                /* If CDATA adding prefered */
                if (RestfulConfig::get('xml_cdata')) {
                    $XMLcontent = '<![CDATA['.$dataValue.']]>';
                } else {
                    $XMLcontent = $dataValue;
                }
                $xmlData .= '
        <'.$dataKey.'>'.$XMLcontent.'</'.$dataKey.'>';
            }
        }

        $xmlData .= '
</'.$requestTable.'>';

        return $this->_response->xml($xmlData);
    }

    /**
     * Return the final response.
     *
     * @param $status_code
     * @param string $data
     * @param string $table_name
     */
    public function response($status_code = 200, $data = 'No detail', $type = null, $isMultiple = false)
    {
        $this->dataType = $type ?: $this->dataType;
        $this->isMultiple = $isMultiple;
        $this->_statusCode = $status_code < 600 ? $status_code : 500;
        if (!in_array($this->_statusCode, array_keys($this->statuses()))) {
            $this->_statusCode = 500;
        }

        // TODO: Check this for if getting serialize errors on windows stemming from here..

        if ($data instanceof \Illuminate\Http\JsonResponse
        || $data instanceof \Illuminate\Http\XmlResponse
        || $data instanceof \Illuminate\Http\RedirectResponse) {
            return $data;
        }

        $response = $this->makeOutput($data, $this->dataType);
        $response->setStatusCode($this->_statusCode);

        return $response;
    }

   /**
    * Get the errors for thei request.
    *
    * @param  [type] $detail [description]
    *
    * @return [type]         [description]
    */
   protected function getErrors($detail)
   {
       $errors = [
           'method' => Request::server('REQUEST_METHOD'),
           'code' => $this->_statusCode,
           'reason' => $this->getStatusMessage(),
           'message' => array_get($detail, 'message', $this->getStatusMessage()),
      ];
       if (\App::environment() == 'dev') {
           $errors['file'] = array_get($detail, 'file');
           $errors['line'] = array_get($detail, 'line');
           $errors['trace'] = array_get($detail, 'trace', debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 20));
       }

       return $errors;
   }
}
