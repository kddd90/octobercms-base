<?php

namespace Nitm\Api\Classes;

use DB;
use Schema;
use Nitm\Api\Models\Configs as RestfulConfig;
use October\Rain\Exception\ApplicationException;

class ApiDeleter
{
    use \Nitm\Api\Traits\ApiTrait;
    /**
     * Holds response data.
     */
    public $data;

    /**
     * Holds REST Class instance.
     */
    public $rest;

    /**
     * Holds all relations.
     */
    public $all_relations = [];

    /**
     * Holds request parameters.
     */
    public $request_parameters = [];

    /**
     * Holds index key.
     */
    public $where_index_key;

    /**
     * Holds index key value.
     */
    public $delete_by_index_key;

    /**
     * Starter method of the component.
     */
    public function __construct()
    {
        $this->rest = Rest::instance();
        $this->type = Trivet::getInputs('req');
    }

    /**
     * Get all relations data.
     *
     * @param $all_relations
     * @param $request_parameters
     * @param $where_index_key
     */
    public function passData($all_relations, $request_parameters, $where_index_key)
    {
        $this->all_relations = $all_relations;
        $this->request_parameters = $request_parameters;

        /* If index key sent as parameter, hold value for next queries */
        $this->delete_by_index_key = Trivet::getInputs($where_index_key) != null ? Trivet::getInputs($where_index_key) : 0;
        $this->where_index_key = $where_index_key;
    }

    /**
     * Check & fetch relation based data before direct table access control.
     */
    public function makeRelationBasedDelete()
    {
        $mapping = $this->all_relations[$this->type];
        if (class_exists(($class = $mapping->relatedtable))) {
            $class::setForApi(true);
            $model = $class::apiFind($this->delete_by_index_key, ['hashedId' => true]);
            if ($model) {
                if (!$model->can('delete')) {
                    throw new ApplicationException(trans('nitm.content::lang.errors.no_permission'), 403);
                }
                $this->data = [
                   'success' => true,
                   'message' => $model->delete(),
                ];
            } else {
                throw new \Exception(trans('nitm.api::lang.responses.no_data', [
                   'indexkey' => '#'.$this->where_index_key.': '.$this->delete_by_index_key,
                   'type' => $this->type,
               ]), 400);
            }
        } else {
            try {
                $this->data = DB::table($this->all_relations[$this->type]->relatedtable)
                ->where($this->where_index_key, $this->delete_by_index_key)
                ->delete();
            } catch (\Exception $e) {
                Trivet::addApiLog(0, 400);
                throw new \Exception(trans('nitm.api::lang.responses.delete_failed', [
                'indexkey' => '#'.$this->where_index_key.': '.$this->delete_by_index_key,
            ]), 400);
            }
        }
    }

    /**
     * Check if direct table access allowed.
     */
    public function makeDirectTableDelete()
    {
        /* Check if direct table access allowed */
        if (RestfulConfig::get('direct_table_output')) {
            /* If requested table exists */
            if (Schema::hasTable($this->type)) {
                $this->data = DB::table($this->type)
                    ->where($this->where_index_key, $this->delete_by_index_key)
                    ->delete();
            }
        } else {
            /* Check if request matches with any relation */
            if (!in_array($this->type, $this->request_parameters)) {
                Trivet::addApiLog(0, 400);
                throw new \Exception(trans('nitm.api::lang.responses.req_mismatch'), 400);
            }
        }
    }

    /**
     * Check for if delete failed.
     */
    public function lastControl()
    {
        /* If delete failed */
        if (!$this->data) {
            Trivet::addApiLog(0, 400);
            throw new \Exception(trans('nitm.api::lang.responses.delete_failed', [
                'indexkey' => '#'.$this->where_index_key.': '.$this->delete_by_index_key,
            ]), 400);
        }

        return trans('nitm.api::lang.responses.delete_ok', [
            'indexkey' => '#'.$this->where_index_key.': '.$this->delete_by_index_key,
        ]);
    }
}
