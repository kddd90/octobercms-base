<?php

namespace Nitm\Api\Classes;

use DB;
use Schema;
use Nitm\Api\Models\Configs as RestfulConfig;
use Nitm\Api\Helpers\Cache;
use October\Rain\Exception\ApplicationException;

class ApiReaderAll extends BaseAction
{
    /**
     * Get all relations data.
     *
     * @param $all_relations
     * @param $request_parameters
     */
    public function passData($all_relations, $request_parameters)
    {
        $this->all_relations = $all_relations;
        $this->request_parameters = $request_parameters;
    }

    /**
     * If "result_limit" value entered bigger than 0 on relation basis..
     */
    public function defineResultLimit()
    {
        $this->result_limit = $this->result_limit ?: 10;
        if (in_array($this->type, $this->request_parameters)) {
            if ($this->all_relations[$this->type]->result_limit > 0) {
                $this->result_limit = $this->all_relations[$this->type]->result_limit;
            }
        }
    }

    /**
     * Check & fetch relation based data before direct table access control.
     */
    public function makeRelationBasedRead()
    {
        /* Maybe "id" field not present in database, so select first index key for order by */
        $mapping = $this->all_relations[$this->type];
        $query = static::getQuery($mapping, 'Nitm\Content', true);
        $this->data = Cache::remember(Trivet::getInputs(), $mapping, function ($mapping) use ($query) {
            ini_set('memory_limit', '256M');
            if (!$query->getModel()->can('readAll')) {
                throw new ApplicationException(trans('nitm.content::lang.errors.no_permission'), 403);
            }

            $model = $query->paginate($this->result_limit, Trivet::getInputs('page') ?? 1);
            if ($model instanceof \Illuminate\Pagination\LengthAwarePaginator) {
                $data = $model->getCollection()->toArray();
            } else {
                $data = $model->toArray();
            }
            // $data['pagination'] = Rest::getPaginationFromCollection($model);

            return [
               '__multiple' => true,
               '__cacheClass' => get_class($query->getModel()),
               '__cacheData' => $data,
            ];
        }, !$query->getModel()->shouldCache ? -1 : null, true, $this->shouldRefresh($mapping));
    }

    /**
     * Check if direct table access allowed.
     */
    public function makeDirectTableRead()
    {
        if (RestfulConfig::get('direct_table_output')) {
            $mapping = new \stdClass();
            $mapping->relatedtable = $this->type;
            $this->data = \Cache::remember(Trivet::getInputs(), $mapping, function () use ($mapping) {
                /* If requested table exists */
               if (Schema::hasTable($mapping->relatedtable)) {
                   /* Maybe "id" field not present in database, so select first index key for order by */
                   $tableName = $mapping->relatedtablee;
                   $orderByIndexKey = Trivet::getPrimaryKey($tableName);

                   return [
                      '__cacheClass' => null,
                      '__cacheData' => DB::table($tableName)
                       ->orderBy($orderByIndexKey)
                       ->get(),
                    ];
               }
            }, null, true);
        } else {
            /* Check if request matches with any relation */
            if (!in_array($this->type, $this->request_parameters)) {
                Trivet::addApiLog(0, 400);
                throw new \Exception(trans('nitm.api::lang.responses.req_mismatch'), 400);
            }
        }
    }

    /**
     * Check for returned data from database.
     */
    public function lastControl()
    {
        /* If no data returned from database */
        if (!$this->data) {
            $this->data = [];
            // throw new \Exception(trans('nitm.api::lang.responses.no_data_all'), 404);
        }

        return $this->data;
    }
}
