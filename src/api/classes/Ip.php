<?php namespace Nitm\Api\Classes;

use October\Rain\Exception\ApplicationException;
use October\Rain\Support\Traits\Singleton;

class Ip
{
    use Singleton;

    /**
     * Determine if client's ip is IPv4 or IPv6 and returns ver. number
     *
     * @param  [type] $ip  [description]
     * @return int [type]  [description]
     */
    public function getIpVersion($ip)
    {
        return strpos($ip, ":") === false ? 4 : 6;
    }

    /**
     * An improved and kind of reliability way of getting the client's IPv4 or IPv6 Address
     * Out of box alternative is Request::ip() (for IPv4)
     *
     * Source: https://www.chriswiegman.com/2014/05/getting-correct-ip-address-php/
     *
     * TODO: Add IPv6 check
     * @return mixed [type]  [description]
     */
    public function realIp()
    {
        // Just get the headers if we can or else use the SERVER global
        if (function_exists('apache_request_headers')) {
            $headers = apache_request_headers();
        } else {
            $headers = $_SERVER;
        }

        // Get the forwarded IPv6 if it exists
        if ( array_key_exists('X-Forwarded-For', $headers) && filter_var($headers['X-Forwarded-For'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV6) ) {
            $realIP = $headers['X-Forwarded-For'];
        }
        // Get the forwarded IPv4 if it exists
        elseif ( array_key_exists('X-Forwarded-For', $headers) && filter_var($headers['X-Forwarded-For'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4) ) {
            $realIP = $headers['X-Forwarded-For'];
        }
        // Get the HTTP_X_FORWARDED_FOR IPv6 if it exists
        elseif ( array_key_exists('HTTP_X_FORWARDED_FOR', $headers) && filter_var($headers['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV6) ) {
            $realIP = $headers['HTTP_X_FORWARDED_FOR'];
        }
        // Get the HTTP_X_FORWARDED_FOR IPv4 if it exists
        elseif ( array_key_exists('HTTP_X_FORWARDED_FOR', $headers) && filter_var($headers['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4) ) {
            $realIP = $headers['HTTP_X_FORWARDED_FOR'];
        }
        // Get the REMOTE_ADDR IPv6 if it exists
        elseif ( filter_var($_SERVER['REMOTE_ADDR'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV6) ) {
            $realIP = filter_var($_SERVER['REMOTE_ADDR'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV6);
        }
        // Get the REMOTE_ADDR IPv4 lastly..
        else {
            $realIP = filter_var($_SERVER['REMOTE_ADDR'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4);
        }

        return $realIP;
    }

    /**
     * Converts ip to number format (IPv4)
     *
     * @param  [type] $ip  [description]
     * @return string [type]  [description]
     * @throws ApplicationException
     */
    public function ipv4_toNumber($ip)
    {
        $pton = inet_pton($ip);

        if (!$pton)
            throw new ApplicationException(trans('nitm.api::lang.exceptions.ip_not_recognizable'));

        $number = '';
        foreach (unpack('C*', $pton) as $byte) {
            $number .= str_pad(decbin($byte), 8, '0', STR_PAD_LEFT);
        }

        return base_convert(ltrim($number, '0'), 2, 10);
    }

    /**
     * Function to determine if an IP is located in a
     * specific range as specified via several alternative formats.
     *
     * This function takes 2 arguments, an IP address and a "range" in several
     * different formats.
     *
     * Network ranges can be specified as:
     * 1. Wildcard format:     1.2.3.*
     * 2. CIDR format:         1.2.3/24  OR  1.2.3.4/255.255.255.0
     * 3. Start-End IP format: 1.2.3.0-1.2.3.255
     *
     * The function will return true if the supplied IP is within the range.
     * Note little validation is done on the range inputs - it expects you to
     * use one of the above 3 formats.
     *
     * Copyright 2008: Paul Gregg <pgregg@pgregg.com>
     * 10 January 2008
     * Version: 1.2
     *
     * Source website: http://www.pgregg.com/projects/php/ip_in_range/
     * Version 1.2
     *
     * This software is Donationware - if you feel you have benefited from
     * the use of this tool then please consider a donation. The value of
     * which is entirely left up to your discretion.
     * http://www.pgregg.com/donate/
     *
     * Please do not remove this header, or source attibution from this file.
     *
     * @param  [type]  $ip    [description]
     * @param  [type]  $range [description]
     * @return boolean        [description]
     */
    public static function ipv4_between($ip, $range)
    {
        if ($ip == $range)
            return true;

        if (strpos($range, '/') !== false) {
            // $range is in IP/NETMASK format
            list($range, $netmask) = explode('/', $range, 2);
            if (strpos($netmask, '.') !== false) {
                // $netmask is a 255.255.0.0 format
                $netmask = str_replace('*', '0', $netmask);
                $netmask_dec = ip2long($netmask);

                return ( (ip2long($ip) & $netmask_dec) == (ip2long($range) & $netmask_dec) );
            } else {
                // $netmask is a CIDR size block
                // fix the range argument
                $x = explode('.', $range);
                while (count($x) < 4) $x[] = '0';
                list($a, $b, $c, $d) = $x;
                $range = sprintf("%u.%u.%u.%u", empty($a) ? '0' : $a, empty($b) ? '0' : $b, empty($c) ? '0' : $c, empty($d) ? '0' : $d);
                $range_dec = ip2long($range);
                $ip_dec = ip2long($ip);

                ## Strategy 1 - Create the netmask with 'netmask' 1s and then fill it to 32 with 0s
                // $netmask_dec = bindec(str_pad('', $netmask, '1') . str_pad('', 32-$netmask, '0'));

                ## Strategy 2 - Use math to create it
                $wildcard_dec = pow(2, (32 - $netmask)) - 1;
                $netmask_dec = ~ $wildcard_dec;

                return (($ip_dec & $netmask_dec) == ($range_dec & $netmask_dec));
            }
        } else {
            // range might be 255.255.*.* or 1.2.3.0-1.2.3.255
            // a.b.*.* format
            if (strpos($range, '*') !== false) {
                // Just convert to A-B format by setting * to 0 for A and 255 for B
                $lower = str_replace('*', '0', $range);
                $upper = str_replace('*', '255', $range);
                $range = "$lower-$upper";

                // return ( (ip2long($ip) > ip2long($lower)) && ip2long($ip) < ip2long($upper) );
            }

            // A-B format
            if (strpos($range, '-') !== false) {
                list($lower, $upper) = explode('-', $range, 2);
                $lower_dec = (float) sprintf("%u", ip2long($lower));
                $upper_dec = (float) sprintf("%u", ip2long($upper));
                $ip_dec = (float) sprintf("%u", ip2long($ip));
                return ( ($ip_dec >= $lower_dec) && ($ip_dec <= $upper_dec) );
            }

            // TODO: 'Range argument is not in 1.2.3.4/24 or 1.2.3.4/255.255.255.0 format';
            return false;
        }
    }

    /**
     * Applies ip2long logic by using gmp or bcmath for IP's based on ipv6
     *
     * Source: https://stackoverflow.com/questions/18276757/php-convert-ipv6-to-number/19497446#19497446
     *
     * @param  [type] $ip [description]
     * @return bool|string [type]  [description]
     */
    public static function ipv6_2long($ip)
    {
        $ip_n = inet_pton($ip);
        $bin = '';

        for ($bit = strlen($ip_n) - 1; $bit >= 0; $bit--) {
            $bin = sprintf('%08b', ord($ip_n[$bit])).$bin;
        }

        if (function_exists('gmp_init')) {
            return gmp_strval(gmp_init($bin, 2), 10);
        } elseif (function_exists('bcadd')) {
            $dec = '0';
            for ($i = 0; $i < strlen($bin); $i++) {
                $dec = bcmul($dec, '2', 0);
                $dec = bcadd($dec, $bin[$i], 0);
            }
            return $dec;
        } else {
            return trigger_error('GMP or BCMATH extension must be installed!', E_USER_ERROR);
        }
    }

    /**
     * Converts inet_pton output to string with bits
     *
     * Source: https://stackoverflow.com/questions/7951061/matching-ipv6-address-to-a-cidr-subnet/7951507#7951507
     *
     * @param  [type] $inet [description]
     * @return string [type]  [description]
     */
    public static function ipv6_inetToBits($inet)
    {
        $unpacked = unpack('A16', $inet);
        $unpacked = str_split($unpacked[1]);
        $binaryip = '';

        foreach ($unpacked as $char) {
            $binaryip .= str_pad(decbin(ord($char)), 8, '0', STR_PAD_LEFT);
        }

        return $binaryip;
    }

    /**
     * Function to determine if an IPv6 is located in a specific range as specified via several alternative formats.
     *
     * Max 8 blocks with 4 digit
     * Every block has 16 subnet prefixes (16 * 8 = 128)
     *
     * @param  string $ip [description]
     * @param  string $range [description]
     * @return bool [description]
     * @throws ApplicationException
     */
    public static function ipv6_between($ip, $range)
    {
        // Expand Ipv6
        // http://svn.kd2.org/svn/misc/libs/tools/ip_utils.php
        $ip = implode(':', str_split(bin2hex(inet_pton($ip)), 4));

        // CIDR Notation: 0:1:a:b:c:d:e:f/24 -- OR -- 0:1:a:b:c:d:e:f/128 -- OR -- 0::1/24 -- OR -- 0:0:1::/64
        if (strpos($range, '/') !== false) {
            $cidrNet = $range;

            $ip = inet_pton($ip);
            $binaryip = self::ipv6_inetToBits($ip);

            list($net, $maskbits) = explode('/', $cidrNet);
            $net = inet_pton($net);
            $binarynet = self::ipv6_inetToBits($net);

            $ip_net_bits = substr($binaryip, 0, $maskbits);
            $net_bits = substr($binarynet, 0, $maskbits);

            // Not in subnet
            if ($ip_net_bits !== $net_bits)
                throw new ApplicationException(trans('nitm.api::lang.exceptions.ip_not_in_subnet'));

            // In subnet
            return true;
        } elseif (strpos($range, '*') !== false || strpos($range, '-') !== false) {
            // Wildcards: a.f.*.* -- OR -- a.b.f.*
            if (strpos($range, '*') !== false) {
                $lower = str_replace('*', '0', $range);
                $upper = str_replace('*', 'f', $range);
            }

            // Range: a.b.c.0-a.b.c.f
            if (strpos($range, '-') !== false)
                list($lower, $upper) = explode('-', $range, 2);

            return (self::ipv6_2long($ip) >= self::ipv6_2long($lower) && self::ipv6_2long($ip) <= self::ipv6_2long($upper));
        } else {
            // Standart: If Ipv6 and range matches
            if (self::ipv6_2long($ip) == self::ipv6_2long($range))
                return true;

            // If Ipv6 and range not matches
            return false;
        }
    }

}
