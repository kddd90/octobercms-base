<?php namespace Nitm\Api\Classes;

use October\Rain\Support\Str;
use October\Rain\Support\Traits\Singleton;
use System\Classes\PluginManager;


class Sysmodels
{
    use Singleton;

    /**
     * [getModelsNamespaces description]
     *
     * @param  [type] $allModelsParam [description]
     * @return array [type]  [description]
     */
    public function getModelsNamespaces($allModelsParam = null)
    {
        $allModels = ($allModelsParam ? $allModelsParam : $this->getModelsOfPlugins());
        $results = [];

        foreach ($allModels as $pluginName => $models) {
            foreach ($models as $modelName => $modelPath) {
                $modelNS = $this->getUpperNamespace($pluginName)."\\Models\\".(is_object($modelPath) ? $modelName : $modelPath);
                $results[$modelNS] = $modelNS;
            }
        }

        return $results;
    }

    /**
     * [getModelsOfPlugins description]
     *
     * @param  [type] $namespaces [description]
     * @return array [type]  [description]
     */
    public function getModelsOfPlugins($namespaces = null)
    {
        $results = [];

        if (!$namespaces)
            $namespaces = $this->getPluginNamespaces();

        foreach ($namespaces as $namespace => $path) {
            if (is_dir($path.'/models/')) {
                $it = new \RecursiveDirectoryIterator($path.'/models/');

                foreach (new \RecursiveIteratorIterator($it) as $file) {
                    if (strtolower($file->getExtension()) == 'php') {
                        $results[$namespace][$file->getBasename('.php')] = $file;
                    }
                }
            }
        }

        return $results;
    }

    /**
     * [getPluginNamespaces description]
     * @return array [type] [description]
     */
    public function getPluginNamespaces()
    {
        $inst = PluginManager::instance();
        $inst->loadPlugins();

        return $inst->getPluginNamespaces();
    }

    /**
     * [getUpperNamespace description]
     *
     * @param  [type] $namespace [description]
     * @return string [type]  [description]
     */
    public function getUpperNamespace($namespace)
    {
        $namespace = Str::normalizeClassName($namespace);

        $parts = explode('\\', $namespace);
        $slice = array_slice($parts, 1, 2);
        $slice = array_map(function($word) {
            return ucfirst($word);
        }, $slice);

        $namespace = implode('\\', $slice);

        return $namespace;
    } 

}
