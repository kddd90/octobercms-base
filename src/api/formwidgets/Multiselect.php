<?php

namespace Nitm\Api\FormWidgets;

use Backend\Classes\FormWidgetBase;
use Nitm\Api\Classes\Trivet;

class Multiselect extends FormWidgetBase
{
    public function widgetDetails()
    {
        return [
            'name' => 'MultiSelect',
            'description' => 'Select2 based multiple select widget.',
        ];
    }

    public function render()
    {
        $this->prepareVars();

        return $this->makePartial('responsefields_multiselect');
    }

    public function loadAssets()
    {
        $this->addJs('js/multiselect.js');
    }

    public function prepareVars()
    {
        $this->vars['name'] = $this->formField->getName();
        $this->vars['value'] = $this->model->{$this->valueFrom};

        $this->vars['formRelatedFieldSelector'] = $this->formField->arrayName.'-'.$this->model->selectorFieldName;
        $this->vars['tablename'] = ($this->model->relatedtable ? $this->model->relatedtable : $this->model->exporttable);
        $this->vars['dbFields'] = Trivet::getFieldsFromDbOrClass($this->vars['tablename'], true);
        $this->vars['dbIndexes'] = Trivet::getIndexesFromDbOrClass($this->vars['tablename']);

        $this->vars['formResponseFieldId'] = $this->formField->arrayName.'-'.$this->formField->fieldName;
    }
}
